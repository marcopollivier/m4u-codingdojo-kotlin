package dojo20170816

import org.junit.Test
import java.util.*
import kotlin.test.assertEquals

class TestExamples {

    fun testRuns(data: IntArray): Int {
        return calcRun(list = data)
    }

    @Test fun testRuns1() {
        test(0)
    }

    @Test fun testRuns2() {
        test(1, 1)
    }

    @Test fun testRuns3() {
        test(3, 1, 2, 3)
    }

    @Test fun testRuns4() {
        test(3, 1, 2, 2, 3)
    }

    @Test fun testRuns5() {
        test(3, 1, 1, 2, 3)
    }

    @Test fun testRuns6() {
        test(1, 1, 1, 1, 1)
    }

    @Test fun testRuns7() {
        test(3, 1, 1, 1, 0, 1, 1)
    }

    @Test fun testRuns8() {
        test(3, 1, 1, 1, 0, 1)
    }

    @Test fun testRuns9() {
        test(5, 1, 0, 1, 0, 1)
    }

    fun calcRun(runs: Int = 0, current: Int? = null, list: IntArray?): Int {
        if (list == null || list.isEmpty()) return runs

        if (current == null && list.size == 1) return 1

        return calcRun(runs = if (current != list.first()) runs + 1 else runs, current = list.first(),
                list = list.copyOfRange(1, list.size))
    }

    fun test(expected: Int, vararg data: Int) {
        assertEquals(expected, testRuns(data), "\ndata = ${Arrays.toString(data)}")
    }
}